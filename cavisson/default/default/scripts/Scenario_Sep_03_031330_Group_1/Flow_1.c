/*-----------------------------------------------------------------------------
    Name: Flow_1
    Created By: This is an auto generated script. You can, however, make a copy of this script using advanced script manager and enhance it
    Date of creation: 4.14.0 (build# 34)
    Flow details:
    Build details: 09/03/2024 08:13:52
    Modification History:
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ns_string.h"

void Flow_1()
{
    ns_start_transaction("index");
    ns_web_url("index",
        "URL=https://www.amazon.in/",
        "METHOD=GET",
    );
    ns_end_transaction("index", NS_AUTO_STATUS);


    ns_page_think_time(0);

}
