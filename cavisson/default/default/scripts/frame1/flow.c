/*-----------------------------------------------------------------------------
    Name: flow
    Created By: cavisson
    Date of creation: 09/02/2024 12:43:29
    Flow details:
    Build details: 4.14.0 (build# 35)
    Modification History:
-----------------------------------------------------------------------------*/
/* Notes :
    There are few additional arguments in all click and script APIs which can be used based on the need:
    
    #NetworkIdleTimeout=<network_idle_timeout_ms>
    The network idle timeout, measured in milliseconds (ms), has a default value of 1500 ms. 
    If the onLoad event is triggered, and there is no network activity for this duration, the page will be considered as fully loaded. 
    It may be necessary to adjust this setting when the expected time gap between two calls exceeds the default value of 1500 ms.

    #PageLoadTimeout=<page_load_timeout_sec>
    This defines the maximum waiting time in seconds for a VUser (Virtual User) to wait for a page to load.
    If the page doesn't load within this duration, the page loading will be aborted, and any captured requests up to that point will be saved. 
    This feature is particularly useful when a specific page's load time exceeds the configured limit in the scenario. 
    The default value in the scenario is 60 seconds, which can be adjusted, but it will apply to all steps and pages.

    #VisualIdleTimeout=<visual_idle_timeout_ms>
    This parameter sets the visual idle timeout in milliseconds, with a default value of 1500 ms. After the network becomes idle, the VUser will wait for visual changes to stabilize.
    If there are no visual changes within this window of time after the network becomes idle, the page will be marked as visually complete. 
    This functionality proves valuable when a specific page's load time exceeds the configured limit in the scenario. 
    The default value in the scenario is 60 seconds, which can be adjusted, though it will apply to all steps and pages.

    #AuthCredential=<username>:<password>
    Provide authentication credentials in the format <username>:<password>. AuthCredential is used to supply authentication credentials for websites that require login. 
    The credentials (username and password) are passed in the first API request, either in ns_browser or ns_web_url.

    #Optional=<0 or 1>
    Optional is a flag used to prevent a specific transaction from affecting the rest of the transactions. 
    For instance, in cases involving popups, if the popup doesn't appear, the transaction could fail, disrupting the entire test. By setting Optional to 1, the test will continue even if the transaction fails.

    #heroElementID=<element id selector>
    #heroElementXPATH=<element xpath selector>	
    #heroElementXPATH1=<alternative element xpath selector>
    #heroElementCSSPATH=<csspath selector>	
    #heroElementDOMSTRING=<domstring>
    #heroElementMark=<value>
    #heroElementMeasure=<value>
    Visual progress is considered complete when the HeroElement becomes visible on the screen. Users can specify any element as the hero element using the above arguments.
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ns_string.h"

void flow()
{

    ns_start_transaction("index_html");
    ns_browser("index_html",
        "url=https://10.10.50.2:7909/frame/index.html",
        "Navigation=1",
        "browserurl=https://10.10.50.2:7909/frame/index.html",
        "action=Home",
        "title=Original Page",
        "Snapshot=webpage_1725255790799.png");
    ns_end_transaction("index_html", NS_AUTO_STATUS);

    ns_page_think_time(6.479);



    ns_start_transaction("clickGotoNewPage_2");
    ns_button("clickGotoNewPage_2",
        "url=https://10.10.50.2:7909/frame/index.html",
        "Navigation=0",
        "content=Go to New Page",        
        "action=click",
        attributes=["onclick=openNewPage()"],
        "csspath=button",
        "xpath=//BUTTON[normalize-space(text())=\"Go to New Page\"]",
        "xpath1=//TITLE[normalize-space(text())=\"Original Page\"]/../../BODY[1]/BUTTON[1]",
        "xpath2=HTML/BODY[1]/BUTTON[1]",
        "tagName=BUTTON",
        "Snapshot=webpage_1725255797285.png");
    ns_end_transaction("clickGotoNewPage_2", NS_AUTO_STATUS);

    ns_page_think_time(9.797);


    ns_page_think_time(9.797);

    ns_start_transaction("clickCloseThisPage_2");
    ns_button("clickCloseThisPage_2",
        "url=https://10.10.50.2:7909/frame/newpage.html",
        "Navigation=1",
        "content=Close This Page",        
        "action=click",
        attributes=["onclick=closePage()"],
        "csspath=button",
        "xpath=//BUTTON[normalize-space(text())=\"Close This Page\"]",
        "xpath1=//TITLE[normalize-space(text())=\"New Page\"]/../../BODY[1]/BUTTON[1]",
        "xpath2=HTML/BODY[1]/BUTTON[1]",
        "tagName=BUTTON",
        "Snapshot=webpage_1725255807082.png");
    ns_end_transaction("clickCloseThisPage_2", NS_AUTO_STATUS);

    ns_page_think_time(2.348);


}
