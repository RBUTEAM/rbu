/*-----------------------------------------------------------------------------
    Name: exit_script
    Created By: cavisson
    Date of creation: 4.14.0 (build# 35)
    Flow details:
    Build details: 09/02/2024 12:43:29
    Modification History:
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include "ns_string.h"

int exit_script ()
{
    return 0;
}
