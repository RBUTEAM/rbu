/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: Brajesh
    Date of recording: 05/25/2023 11:24:41
    Flow details:
    Build details: 4.11.0 (build# 49)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{

    ns_start_transaction("CreateUser");
    ns_web_url ("CreateUser",
        "URL=https://127.0.0.1/cav/samples/petstore?QP_operation=createUser",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"104\"",
        "HEADER=accept:application/json",
        "HEADER=Content-Type:application/json",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Origin:https://127.0.0.1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "BODY=$CAVINCLUDE$=createUser.body"
    );

    ns_end_transaction("CreateUser", NS_AUTO_STATUS);
    ns_page_think_time(0);
    
    ns_start_transaction("loginUser");
    ns_web_url ("loginUser",
        "URL=https://127.0.0.1/cav/samples/petstore?QP_operation=loginUser",
         "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"104\"",
        "HEADER=accept:application/json",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
       "BODY=$CAVINCLUDE$=loginUser.body"
    
    );

    ns_end_transaction("loginUser", NS_AUTO_STATUS);
    ns_page_think_time(0);

    
    ns_start_transaction("addPet");
    ns_web_url ("addPet",
        "URL=https://127.0.0.1/cav/samples/petstore?QP_operation=addPet",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"104\"",
        "HEADER=accept:application/json",
        "HEADER=Content-Type:application/json",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Origin:https://127.0.0.1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "BODY=$CAVINCLUDE$=addPet.body"
    );

    ns_end_transaction("addPet", NS_AUTO_STATUS);
    ns_page_think_time(0);

    
    ns_start_transaction("findByStatus");
    ns_web_url ("findByStatus",
        "URL=https://127.0.0.1/cav/samples/petstore?QP_operation=findPetsByStatus&QP_PetsStatus=available",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"104\"",
        "HEADER=accept:application/json",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
          );

    ns_end_transaction("findByStatus", NS_AUTO_STATUS);
    ns_page_think_time(0);

    
    ns_start_transaction("FindByPetId");
    ns_web_url ("FindByPetId",
        "URL=https://127.0.0.1/cav/samples/petstore?QP_operation=getPetById",
         "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"104\"",
        "HEADER=accept:application/json",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
       "BODY=$CAVINCLUDE$=FindByPetId.body"
    );

    ns_end_transaction("FindByPetId", NS_AUTO_STATUS);
    ns_page_think_time(0);

    
    ns_start_transaction("inventory");
    ns_web_url ("inventory",
        "URL=https://127.0.0.1/cav/samples/petstore?QP_operation=getInventory",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"104\"",
        "HEADER=accept:application/json",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
    
    );

    ns_end_transaction("inventory", NS_AUTO_STATUS);
    ns_page_think_time(0);

   
    ns_start_transaction("order");
    ns_web_url ("order",
        "URL=https://127.0.0.1/cav/samples/petstore?QP_operation=placeOrder",
        "METHOD=POST",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"104\"",
        "HEADER=accept:application/json",
        "HEADER=Content-Type:application/json",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Origin:https://127.0.0.1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9",
        "BODY=$CAVINCLUDE$=order.body"
    );

    ns_end_transaction("order", NS_AUTO_STATUS);
    ns_page_think_time(0);

    
    ns_start_transaction("logout");
    ns_web_url ("logout",
        "URL=https://127.0.0.1/cav/samples/petstore?QP_operation=logoutUser",
        "HEADER=sec-ch-ua:\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"104\"",
        "HEADER=accept:application/json",
        "HEADER=sec-ch-ua-mobile:?0",
        "HEADER=sec-ch-ua-platform:\"Linux\"",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Sec-Fetch-Dest:empty",
        "HEADER=Accept-Language:en-US,en;q=0.9"
    );

    ns_end_transaction("logout", NS_AUTO_STATUS);
    ns_page_think_time(0);

}
