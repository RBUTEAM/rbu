/*-----------------------------------------------------------------------------
    Name: exit_script
    Generated By: netstorm
    Date of generation: 05/11/2023 05:19:30
    Flow details:
    Build details: 4.11.0 (build# 47)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
